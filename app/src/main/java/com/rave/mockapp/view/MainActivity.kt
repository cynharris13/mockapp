package com.rave.mockapp.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.ui.tooling.preview.Preview
import coil.compose.AsyncImage
import com.rave.mockapp.model.MockRepo
import com.rave.mockapp.model.remote.MockServer
import com.rave.mockapp.ui.theme.MockAppTheme
import com.rave.mockapp.viewmodel.MainViewModel

class MainActivity : ComponentActivity() {
    private val mainViewModel by viewModels<MainViewModel> {
        MainViewModel.getFactory(MockRepo(MockServer.getInstance()))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MockAppTheme {
                val result: List<String> by mainViewModel.urls.collectAsState()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ImageList(result)
                }
            }
        }
    }
}

@Composable
fun ImageList(urls: List<String>) {
    LazyColumn {
        items(urls) { url -> ListRow(url) }
    }
}

@Composable
fun ListRow(url: String)
{
    //Text("An image should be here!")
    Row (modifier = Modifier.fillMaxWidth()){
        AsyncImage(
            model = url,
            contentDescription = "Image",
            modifier = Modifier.fillMaxSize()
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MockAppTheme {
        ImageList(listOf(
            "https://cdn.shibe.online/shibes/a2c5461e2638c0f24b14746398ec6394413aebc7.jpg",
            "https://cdn.shibe.online/shibes/5f4e3396cce09a178b5d6c153724f0126426bd28.jpg",
            "https://cdn.shibe.online/shibes/f9013c123132cd45517c43cbd3d061a60df44c1a.jpg"))
    }
}