package com.rave.mockapp.model

import com.rave.mockapp.model.remote.MockServer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class MockRepo(private val mockServer: MockServer) {

    suspend fun getImages(): List<String> {
        return withContext(Dispatchers.IO) {
            mockServer.getImages()
        }
    }
}
