package com.rave.mockapp.model.remote

class MockServerImpl : MockServer {
    override suspend fun getImages(): List<String> {
        return listOf(
            "https://cdn.shibe.online/shibes/60c5c8b8e9b038205b13b7b00b159e6f0dcd09bb.jpg",
            "https://cdn.shibe.online/shibes/a1eb74e429e7ad225d4b7c7a48bd1184b188b5d5.jpg"
        )
    }
}