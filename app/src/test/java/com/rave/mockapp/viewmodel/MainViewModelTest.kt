package com.rave.mockapp.viewmodel

import com.rave.mockapp.model.MockRepo
import com.rave.mockapp.model.utilTest.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class MainViewModelTest {

    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()

    private val repo = mockk<MockRepo>()

    @Test
    fun testGetUrlsOnInit() {
        // Given
        val urls = listOf("url")
        coEvery { repo.getImages() } coAnswers { urls }

        // When
        val mainViewModel = MainViewModel(repo)

        // Then
        Assertions.assertEquals(urls, mainViewModel.urls.value)
    }
}