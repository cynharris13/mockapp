package com.rave.mockapp.model

import com.rave.mockapp.model.remote.MockServer
import com.rave.mockapp.model.utilTest.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class MockRepoTest {

    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()
    private val mockServer = mockk<MockServer>()
    private val repo = MockRepo(mockServer)


    @Test
    @DisplayName("Testing that a list of images is returned")
    fun testGetImages() = runTest(coroutinesTestExtension.dispatcher) {
        // Given
        val result = listOf("yahooo")
        coEvery { mockServer.getImages() } coAnswers { result}

        // When
        val images = repo.getImages()

        // Then
        Assertions.assertEquals(result, images)
    }
}